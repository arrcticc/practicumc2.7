# practicumc2.7


Результаты выполнения практикума можно посмотреть здесь:

[](https://)

[My prometeus](http://34.118.67.122:3000/d/wX0WPuU7k/praktikum-s2-7?orgId=1&from=now-1h&to=now)

```
Login - SkillFactory

Password - SkillFactory
```

В данном репозитории лежат все файлы практикума.

Можно клонировать и запустить.

1. **Добавил *Node Exporter* и *BlackBox Exporter***

   ![](screenshots/13.06.21.png)
2. **Собрал метрики со своего хоста и http://lms.skillfactory.ru и построил дашборд**

   ![](screenshots/15.31.40.png)
3. Добавил алерты на следующие события:

* изменился статус-код сайта *[lms.skillfactory.ru](https://lms.skillfactory.ru/),*
* задержка превышает 5 секунд *[lms.skillfactory.ru](https://lms.skillfactory.ru/)* *,*
* сервер перезагрузился (через метрику ***Uptime*** ).

  ![](screenshots/16.43.57.png)

  ![](screenshots/16.48.55.png)

  ![](screenshots/16.55.39.png)

4. Для того, чтобы настроить отправку алертов, изменил docker-compose.yml - для того, чтобы подгружать конфиг *grafana.ini* с параметрами smtp-servera.

![](screenshots/16.37.51.png)

Добавил алерты для своего бота в Телеграмме, и сделал мультипойнт для рассылки алертов

![](screenshots/16.37.02.png)


Оповещения приходят.


![](screenshots/17.03.57.png)
